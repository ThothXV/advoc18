(ql:quickload "alexandria")

(defparameter *inlist*
  (with-open-file (f "infixed")
    (loop for x = (read f nil)
       until (not x)
       collect x)))

(defun bits (il)
  (remove-duplicates (alexandria:flatten il)))

(defun depset (il)
  (let ((bits (bits il))
        (depord (mapcar #'reverse il)))
    (loop for b in bits
       collect (cons b
                     (mapcar #'cadr
                             (remove-if-not
                              (lambda (dep) (eq (car dep) b)) depord))))))

(defun pickdep (deps)
  (car (sort (mapcar #'car (remove-if #'cdr deps))
             #'string-lessp :key #'symbol-name)))

(defun remove-dep (dep deps)
  (remove dep deps :key #'car))

(defun complete-dep (dep deps)
  (remove-dep dep
              (mapcar (lambda (dl)
                        (cons (car dl)
                              (remove dep (cdr dl))))
                      deps)))

(defun part-a (il)
  (let ((deps (depset il))
        (res nil))
    (loop while deps
       do (progn
            (setf res
                  (cons (pickdep deps)
                        res))
            (setf deps (complete-dep (car res) deps)))
         finally (return (reverse res)))))

(defun part-time (p)
  (+ 61 (- (char-code (char (symbol-name p) 0)) (char-code #\A))))

(defstruct worker
  (part nil)
  (td 0))

(defun make-workers (n)
  (loop for i from 1 to n collect (make-worker)))

(defun processingp (workers)
  (not (every #'zerop (mapcar #'worker-td workers))))

(defun tick (workers)
  (mapc (lambda (w)
          (unless (zerop (worker-td w))
            (decf (worker-td w))))
        workers))

(defun complete-tasks (workers deps)
  (mapc (lambda (w) (setf deps (complete-dep (worker-part w) deps)))
        (remove-if-not #'zerop workers :key #'worker-td))
  deps)

(defun assign-tasks (workers deps)
  (mapc (lambda (w)
          (when (setf (worker-part w) (pickdep deps))
            (setf deps (remove-dep (worker-part w) deps))
            (setf (worker-td w) (part-time (worker-part w)))))
        (remove-if-not #'zerop workers :key #'worker-td))
  deps)

(defun part-b (il)
  (let ((deps (depset il))
        (workers (make-workers 5)))
    (loop while (or deps (processingp workers))
       counting t into time
       do (progn
            (tick workers)
            (setf deps (complete-tasks workers deps))
            (setf deps (assign-tasks workers deps)))
       finally (return (1- time)))))
