#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#define PLAYERS 416
#define MAXMARB 7161700

uint64_t scores[PLAYERS] = {0};

typedef struct circ_s{
    struct circ_s* clock;
    struct circ_s* counter;
    int s;
} circ_t;

circ_t* make_circ(int iv){
    circ_t *c = calloc(1, sizeof(circ_t));
    c->s = iv;
    c->clock = c;
    c->counter = c;
    return c;
}

void free_circ(circ_t* c){
    circ_t *f=c->clock;
    while(f!=c){
        circ_t* fn=f->clock;
        free(f);
        f=fn;
    }
    free(c);
}

void insert_clock(circ_t* c, int n){
    circ_t *new = calloc(1, sizeof(circ_t));
    new->s = n;
    circ_t *oclk = c->clock;
    c->clock=new;
    new->counter=c;
    new->clock=oclk;
    oclk->counter=new;
}

void remove_counter(circ_t *c){
    circ_t *cc = c->counter;
    c->counter=cc->counter;
    c->counter->clock=c;
    free(cc);
}

int main(int argc, char **argv){
    circ_t *c = make_circ(0);
    for(int i=1,j=0;i<=MAXMARB;i++,j=(j+1)%PLAYERS){
        if((i%23)!=0){
            c=c->clock;
            insert_clock(c, i);
            c=c->clock;
        }else{
            scores[j]+=i;
            c=c->counter->counter->counter->counter->counter->counter;
            scores[j]+=c->counter->s;
            remove_counter(c);
        }
    }
    uint64_t max=0;
    for(int i=0;i<PLAYERS;i++) max=(scores[i]>max)?scores[i]:max;
    printf("%lld", max);
    free_circ(c);
}
