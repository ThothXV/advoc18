#include <stdio.h>
#include <stdint.h>
#define SERIAL 1133

struct siz{int64_t x;int64_t y;int64_t s;int64_t v;};

int64_t cval(int64_t x, int64_t y){
    return ((((((x+10)*y)+SERIAL)*(x+10))/100)%10)-5;
}

int64_t aval(int64_t x, int64_t y, int64_t s){
    int64_t res=0;
    for(int dx=0;dx<s;dx++)
        for(int dy=0;dy<s;dy++)
            res+=cval(x+dx,y+dy);
    return res;
}

struct siz msiz(int64_t s){
    int64_t max=0, mx=0, my=0;
    for(int x=1;x<=(300-(s-1));x++){
        for(int y=1;y<=(300-(s-1));y++){
            int64_t v = aval(x, y, s);
            if(v>max){
                max=v;
                mx=x;
                my=y;
            }
        }
    }
    return (struct siz){mx,my,s,max};
}

int main(int argc, char **argv){
    struct siz m = (struct siz){0,0,0,0};
    for(int64_t i=1;i<=300;i++){
        struct siz v = msiz(i);
        m=(v.v>m.v)?v:m;
    }
    printf("%ld, %ld (x%ld): %ld", m.x, m.y, m.s, m.v);
}
