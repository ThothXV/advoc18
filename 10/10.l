(defparameter *points*
  (with-open-file (f "infixed")
    (loop for x = (read f nil)
       until (not x)
       collect x)))

(defun applyvel (f points)
  (mapcar (lambda (pv)
            (list (funcall f (car pv) (cadr pv)) (cadr pv)))
          points))

(defun tick (points)
  (applyvel #'+ points))

(defun untick (points)
  (applyvel #'- points))

(defun points-bounds (points)
  (loop for (p v) in points
     minimize (realpart p) into min-x
     maximize (realpart p) into max-x
     minimize (imagpart p) into min-y
     maximize (imagpart p) into max-y
     finally (return (values min-x max-x min-y max-y))))

(defun points-w (points)
  (multiple-value-bind (min-x max-x) (points-bounds points)
    (abs (- max-x min-x))))

(defun points-area (points)
  (multiple-value-bind (min-x max-x min-y max-y) (points-bounds points)
    (abs (* (- max-x min-x) (- max-y min-y)))))

(defun print-points (points)
  (multiple-value-bind (min-x max-x min-y max-y) (points-bounds points)
    (loop for y from max-y downto min-y
       do (loop for x from min-x to max-x
             do (format t "~a" (if (assoc (complex x y) points :test #'=) #\# #\.)))
       do (format t "~%"))))

(defun part-a (points)
  (loop
     for p = points then (tick p)
     and oa = (points-w points) then a
     and a = (points-w points) then (points-w p)
     when (< oa a)
     do (progn
          (loop repeat 10
             for pts = (untick (untick (untick (untick (untick p)))))
             then (tick pts)
             do (print-points p))
          (return-from part-a))))
