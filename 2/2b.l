(defparameter *lines*
  (with-open-file (f "input.txt")
    (loop for l = (read-line f nil)
       until (not l)
       collect l)))

(defun tcodes (s1 s2)
  (= 1 (reduce (lambda (c v) (if v c (+ c 1)))
               (map 'list #'eql s1 s2)
               :initial-value 0)))

(write (block e
         (dolist (s1 *lines*)
           (dolist (s2 *lines*)
             (if (tcodes s1 s2) (return-from e (list s1 s2)))))))
