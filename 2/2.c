#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

int dub = 0;
int trip = 0;
int idx[26];

int main(int argc, char **argv){
    for(int i=0;i<26;i++) idx[i]=0;
    FILE *f = fopen("input.txt", "r");
    if(!f) {printf("File open failed\n"); exit(1);}
    for(char c;(c = getc(f))!=EOF;){
        if(isalpha(c)) idx[c-'a']++;
        if(c=='\n'){
            int d=0, t=0;
            for(int i = 0;i<26;i++){
                if(idx[i]==2) d=1;
                if(idx[i]==3) t=1;
                idx[i]=0;
            }
            dub+=d;trip+=t;
        }
    }
    printf("%d",dub*trip);
}
