(defun killp (a b)
  (and (char-equal a b)
       (not (char= a b))))

(defun kills (l)
  (labels ((kill (fst snd lst)
             (cond ((not snd) (if fst (list fst) nil))
                   ((killp fst snd) (kill (car lst) (cadr lst) (cddr lst)))
                   (t (cons fst (kill snd (car lst) (cdr lst)))))))
    (kill (car l) (cadr l) (cddr l))))

(defun part-a (str)
  (do* ((p (coerce str 'list) s)
        (s (kills (coerce str 'list)) (kills s)))
       ((equal p s) (length s))))

(defun runit (str c)
  (remove c str :test #'char-equal))

(defun part-b (str)
  (loop for c across "abcdefghijklmnopqrstuvwxyz"
       minimize (part-a (runit str c))))
