BEGIN{
    FS="[-:# ]+";
}
{
    if($6=="Guard"){
        if($4 > 0){
            if(($2==9||$2==4||$2==6||$2==11)&&$3==30){$2++;$3=1;}
            else if($2==2&&$3==28){$2++;$3=1;}
            else if($3==31){$2++;$3=1;}
            else{$3++;}
        }
        watch[$2+0 "-" $3+0]["guard"]=$7;
        next;
    }
    watch[$2+0 "-" $3+0][$5+0]=($6=="falls")?1:-1;
}
END{
    sleep=-1;
    for(d in watch){
	if(!watch[d]["guard"]) print "missing guard on " d;
        for(i=0;i<60;i++){
            (watch[d][i]!=0)?
                (sleep=watch[d][i]):
                (watch[d][i]=sleep);
            sins[watch[d]["guard"]]+=(sleep==1);
        }
    }
    sleeper=0;
    for(g in sins)
        if(sins[g] > sins[sleeper]) sleeper=g;

    for(d in watch){
        if(watch[d]["guard"]!=sleeper) continue;
        for(i=0;i<60;i++)
            sleeps[i]+=(watch[d][i]==1);
    }
    smin=0;
    for(s in sleeps)
        if(sleeps[s] > sleeps[smin]) smin=s;
    print (sleeper*smin);
}
