BEGIN{
    FS="[-:# ]+";
}
{
    if($6=="Guard"){
        if($4 > 0){
            if(($2==9||$2==4||$2==6||$2==11)&&$3==30){$2++;$3=1;}
            else if($2==2&&$3==28){$2++;$3=1;}
            else if($3==31){$2++;$3=1;}
            else{$3++;}
        }
        watch[$2+0 "-" $3+0]["guard"]=$7;
        next;
    }
    watch[$2+0 "-" $3+0][$5+0]=($6=="falls")?1:-1;
}
END{
    sleep=-1;
    for(d in watch){
        for(i=0;i<60;i++){
            (watch[d][i]!=0)?
                (sleep=watch[d][i]):
                (watch[d][i]=sleep);
            sins[watch[d]["guard"]][i]+=(sleep==1);
        }
    }
    sleeper=0;
    smin=0
    for(g in sins)
        for(i=0;i<60;i++)
            if(sins[g][i] > sins[sleeper][smin]){sleeper=g;smin=i;};
    
    print (sleeper*smin);
}
