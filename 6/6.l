(defparameter *pts*
  (with-open-file (f "input.l")
    (loop for x = (read f nil)
       until (not x)
       collect x)))

(defun mdist (c1 c2)
  (+ (abs (- (realpart c2) (realpart c1)))
     (abs (- (imagpart c2) (imagpart c1)))))

(defun closest-point (c pts)
  (let* ((md (loop for p in pts
                minimizing (mdist c p)))
         (matches (remove-if-not (lambda (p) (= (mdist c p) md))
                                 pts)))
    (if (= (length matches) 1)
        (car matches)
        nil)))

(defun findrange (pts)
  (loop for p in pts
     maximizing (realpart p) into max-x
     minimizing (realpart p) into min-x
     maximizing (imagpart p) into max-y
     minimizing (imagpart p) into min-y
     finally (return-from findrange (values max-x min-x max-y min-y))))

(defun excludes (pts)
  (multiple-value-bind (max-x min-x max-y min-y) (findrange pts)
    (remove-if-not
     #'identity
     (remove-duplicates
      (loop for x from (1- min-x) to (1+ max-x)
         appending (loop for y from (1- min-y) to (1+ max-y)
                      if (or (< y min-y) (> y max-y)
                             (< x min-x) (> x max-x))
                      collect (closest-point (complex x y) pts)))))))

(defun part-a (pts)
  (multiple-value-bind (max-x min-x max-y min-y) (findrange pts)
    (let ((grid (make-hash-table))
          (excl (excludes pts))
          (sums (make-hash-table)))
      (loop for x from min-x to max-x
         do (loop for y from min-y to max-y
               do (setf (gethash (complex x y) grid)
                        (closest-point (complex x y) pts))))
      (loop for c being the hash-values in grid
         do (if (and c (not (member c excl))) (incf (gethash c sums 0))))
      (loop for a being the hash-values in sums maximizing a))))

(defun mdall (c pts)
  (loop for p in pts
     sum (mdist c p)))

(defun part-b (pts)
  (multiple-value-bind (max-x min-x max-y min-y) (findrange pts)
    (loop for x from min-x to max-x
       sum (loop for y from min-y to max-y
              sum (if (< (mdall (complex x y) pts) 10000) 1 0)))))
