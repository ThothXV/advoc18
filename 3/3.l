(defstruct claim id x y w h)

(defparameter *claims*
  (with-open-file (f "input.l")
    (eval (cons 'list (read f)))))

(defun collisionp (c1 c2)
  (and (< (claim-x c1)
          (+ (claim-w c2) (claim-x c2)))
       (> (+ (claim-w c1) (claim-x c1))
          (claim-x c2))
       (< (claim-y c1)
          (+ (claim-h c2) (claim-y c2)))
       (> (+ (claim-h c1) (claim-y c1))
          (claim-y c2))))

(defun part-a (claims)
  (let ((areas (make-hash-table)))
    (loop for o in claims
       do (loop for x from (claim-x o) to (+ (claim-x o) (1- (claim-w o)))
             do (loop for y from (claim-y o) to (+ (claim-y o) (1- (claim-h o)))
                   do (incf (gethash (complex x y) areas 0)))))
    (loop for n being the hash-values in areas
         sum (if (> n 1) 1 0))))

(defun part-b (claims)
  (dolist (claim claims)
    (if (= 1 (reduce #'+
                     (mapcar
                      (lambda (c) (if (collisionp c claim) 1 0))
                      claims)))
        (return-from part-b (claim-id claim)))))
