#include <stdio.h>
#include <stdlib.h>
typedef struct node_s {
    int nchld;
    int nmds;
    struct node_s** chld;
    int* mds;
} node_t;

int sum_tree(node_t* node){
    int n=0;
    for(int i=0;i<node->nmds;i++)
        n+=node->mds[i];
    for(int i=0;i<node->nchld;i++)
        n+=sum_tree(node->chld[i]);
    return n;
}

node_t* build_tree(){
    node_t* node = calloc(1, sizeof(node_t));
    if(scanf("%d %d", &node->nchld, &node->nmds)==EOF) {free(node); return NULL;}
    if(node->nchld) node->chld = calloc(node->nchld, sizeof(node_t*));
    if(node->nmds) node->mds = calloc(node->nmds, sizeof(int));
    for(int i=0;i<node->nchld;i++){
        node->chld[i]=build_tree();
    }
    for(int i=0;i<node->nmds;i++){
        scanf("%d", &node->mds[i]);
    }
    return node;
};

void free_tree(node_t* node){
    if(node->nmds) free(node->mds);
    if(node->nchld) {
        for(int i=0;i<node->nchld;i++)
            free_tree(node->chld[i]);
    }
    free(node->chld);
    free(node);
}

int value_tree(node_t* node){
    if(!node->nchld){
        int n=0;
        for(int i=0;i<node->nmds;i++)
            n+=node->mds[i];
        return n;
    }
    int n=0;
    for(int i=0; i<node->nmds;i++){
        int idx=node->mds[i]-1;
        if(idx<0||idx>=node->nchld) continue;
        n+=value_tree(node->chld[idx]);
    }
    return n;
}

int main(int argc, char** argv){
    node_t* n = build_tree();
    printf("%d %d\n", sum_tree(n), value_tree(n));
    free_tree(n);
    return 0;
}
